package com.android.gambung_crowdfund;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

public class MasukNominal extends AppCompatActivity {

    TextView nominal;
    int nomin = 0;
    RadioGroup radioGroup;
    BottomSheetDialog dialog;
    View newView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masuk_nominal);
        nominal = findViewById(R.id.TextNominal);
        newView = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet, null);
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(newView);
        radioGroup = findViewById(R.id.radioButtonGroup);
    }

    public void beriDonasi(View view) {

        if(nomin >= 10000 && !nominal.getText().equals(""))
        {
            dialog.show();
            radioGroup = findViewById(R.id.radioButtonGroup);
        }
        else if (nomin>=10000){
            Toast.makeText(MasukNominal.this,"Nominal kurang dari minimal donasi",Toast.LENGTH_SHORT).show();
        }
        else if(nominal.getText().equals("")){
            Toast.makeText(MasukNominal.this,"Masukkan nominal terlebih dahulu",Toast.LENGTH_SHORT).show();
        }
    }

    public void isiNominal(View view){
        switch(view.getId()){
            case(R.id.sepribu):
                nominal.setText("10.000");
                nomin = 10000;
                break;
            case(R.id.duapribu):
                nominal.setText("20.000");
                nomin = 20000;
                break;
            case(R.id.limapribu):
                nominal.setText("50.000");
                nomin = 50000;
                break;
            case(R.id.serribu):
                nominal.setText("100.000");
                nomin = 100000;
                break;
            case(R.id.duarribu):
                nominal.setText("200.000");
                nomin = 200000;
                break;
            case(R.id.limarribu):
                nominal.setText("500.000");
                nomin = 500000;
                break;
        }
    }

    public void konfirmNominal(View view)
    {
        RadioButton rJ = findViewById(R.id.butJi);
        RadioButton rT = findViewById(R.id.butTr);
        if (rJ.isChecked()) {
            Intent intent = new Intent(this, TransferDonasi.class);
            intent.putExtra("nominal", nomin);
            startActivity(intent);
        } else if (rT.isChecked()) {
            Intent intent = new Intent(this, TransferDonasi.class);
            intent.putExtra("nominal", nomin);
            startActivity(intent);
        }
    }

    public void detailDonasi(View view) {
    }
}
